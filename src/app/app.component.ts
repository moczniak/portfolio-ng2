import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { SharedMockService } from './services/sharedMockService';

@Component({
    selector: 'mocz-main',
    templateUrl: 'app/app.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [SharedMockService]
})
export class AppComponent {

}
