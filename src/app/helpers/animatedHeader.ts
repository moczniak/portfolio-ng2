export class AnimatedHeader {

  private _docElem = document.documentElement;
  private	_header = document.querySelector( '.navbar-fixed-top' );
  private	_didScroll = false;
  private	_changeHeaderOn = 300;


  init() {
    window.addEventListener( 'scroll', ( event ) => {
      if ( !this._didScroll ) {
        this._didScroll = true;
        setTimeout(() => this.scrollPage(), 250);
      }
    }, false );
  }


  scrollPage() {
    let sy = this.scrollY();
    if ( sy >= this._changeHeaderOn ) {
      this._header.classList.add('navbar-shrink');
    }else {
      this._header.classList.remove('navbar-shrink');
    }
    this._didScroll = false;
  }

  scrollY() {
    return window.pageYOffset || this._docElem.scrollTop;
  }

}
