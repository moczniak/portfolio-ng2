import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';


@Injectable()
export class MockService {

  constructor( private _http: Http ) { }

  getAbout(): Observable<any[]> {
    return this._http.get('app/mocks/about.json')
      .map(this.extractData)
      .catch(this.handleError);
  }

  getFrameworks(): Observable<any[]> {
    return this._http.get('app/mocks/frameworks.json')
      .map(this.extractData)
      .catch(this.handleError);
  }

  getLanguages(): Observable<any[]> {
    return this._http.get('app/mocks/languages.json')
      .map(this.extractData)
      .catch(this.handleError);
  }


  getAllProjects(): Observable<any[]> {
    return this._http.get('app/mocks/projects.json')
      .map(this.extractData)
      .catch(this.handleError);
  }

  searchForProject(projects: any, ID: number) {
    return projects.find((value) => value.ID === ID );
  }

  private extractData(res: Response) {
    if (res.status < 200 || res.status >= 300) {
      throw new Error('Response status: ' + res.status);
    }
    let body = res.json();
    return body || {};
  }

  private handleError(error: any) {
    let errMsg = error || 'Server Error';
    return Observable.throw(errMsg);
  }

}
