import { Injectable } from '@angular/core';

@Injectable()
export class SharedMockService {

  singleMock: any;

  setSingleMock(mock: any) {
    this.singleMock = mock;
  }

  getSingleMock() {
    return this.singleMock;
  }


}
