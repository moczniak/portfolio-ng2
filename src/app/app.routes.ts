import { provideRouter, RouterConfig } from '@angular/router';


 import { ProjectDescComponent } from './components/project/projectDesc.component';
 import { HomeComponent } from './components/home/home.component';


const routes: RouterConfig = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: ':projectID',
    component: ProjectDescComponent
  }
];

export const APP_ROUTER_PROVIDERS = [
    provideRouter(routes)
];
