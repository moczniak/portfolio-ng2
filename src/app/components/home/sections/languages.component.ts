import { Component, OnInit, OnDestroy } from '@angular/core';
import { HTTP_PROVIDERS }    from '@angular/http';

import { MockService } from './../../../services/mockService';

@Component({
  selector: 'mocz-languages',
  templateUrl: 'app/components/home/sections/languages.template.html',
  providers: [HTTP_PROVIDERS, MockService]
})

export class LanguagesComponent implements OnInit, OnDestroy {

  languages: any;
  counter = Array;

  private sub: any;

  constructor(private _mockService: MockService) {}

  ngOnInit() {
      this.sub = this._mockService.getLanguages()
                .subscribe(
                  languages => this.languages = languages
                );
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }


}
