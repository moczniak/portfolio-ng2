import { Component, OnInit, OnDestroy } from '@angular/core';
import { HTTP_PROVIDERS }    from '@angular/http';

import { MockService } from './../../../services/mockService';

@Component({
  selector: 'mocz-frameworks',
  templateUrl: 'app/components/home/sections/frameworks.template.html',
  providers: [HTTP_PROVIDERS, MockService]
})

export class FrameworksComponent implements OnInit, OnDestroy {

  frameworks: any;
  counter = Array;

  private sub: any;

  constructor(private _mockService: MockService) {}

  ngOnInit() {
      this.sub = this._mockService.getFrameworks()
                .subscribe(
                  frameworks => this.frameworks = frameworks
                );
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
