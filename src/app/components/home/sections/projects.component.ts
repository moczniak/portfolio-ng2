import { Component, OnInit, OnDestroy } from '@angular/core';
import { HTTP_PROVIDERS }    from '@angular/http';
import { ROUTER_DIRECTIVES } from '@angular/router';

import { MockService } from './../../../services/mockService';
import { SharedMockService } from './../../../services/sharedMockService';

@Component({
  selector: 'mocz-projects',
  templateUrl: 'app/components/home/sections/projects.template.html',
  directives: [ROUTER_DIRECTIVES],
  providers: [HTTP_PROVIDERS, MockService]
})

export class ProjectsComponent implements OnInit, OnDestroy {

  projects: any;

  private sub: any;

  constructor(private _mockService: MockService, private _sharedMockService: SharedMockService) {}

  ngOnInit() {
      this.sub = this._mockService.getAllProjects()
                .subscribe(
                  projects => this.projects = projects
                );
  }

  setMock(project: any) {
    this._sharedMockService.setSingleMock(project);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }




}
