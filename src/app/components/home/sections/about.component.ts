import { Component, OnInit, OnDestroy } from '@angular/core';
import { HTTP_PROVIDERS }    from '@angular/http';

import { MockService } from './../../../services/mockService';

@Component({
  selector: 'mocz-about',
  templateUrl: 'app/components/home/sections/about.template.html',
  providers: [HTTP_PROVIDERS, MockService]
})

export class AboutComponent implements OnInit, OnDestroy {

  about: any;

  private sub: any;

  constructor(private _mockService: MockService) {}

  ngOnInit() {
    this.sub =  this._mockService.getAbout()
                .subscribe(
                  about => this.about = about
                );
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
