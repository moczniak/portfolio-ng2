import { Component, OnInit } from '@angular/core';

 import { PageScroll, PageScrollConfig } from 'ng2-page-scroll';

import { ProjectsComponent } from './sections/projects.component';
import { AboutComponent } from './sections/about.component';
import { LanguagesComponent } from './sections/languages.component';
import { FrameworksComponent } from './sections/frameworks.component';
import { FooterComponent } from './sections/footer.component';

import { AnimatedHeader } from './../../helpers/animatedHeader';

@Component({
  selector: 'mocz-home',
  templateUrl: 'app/components/home/home.template.html',
  directives: [PageScroll, ProjectsComponent, AboutComponent, LanguagesComponent, FrameworksComponent, FooterComponent]
})

export class HomeComponent implements OnInit {
  constructor() {
    PageScrollConfig.defaultDuration = 1500;
    PageScrollConfig.defaultEasingFunction = (t: number, b: number, c: number, d: number): number => {
        // easeInOutExpo easing
        if (t === 0) {
          return b;
        }
        if (t === d) {
          return b + c;
        }
        if ((t /= d / 2) < 1) {
          return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
        }
        return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
    };
  }

  ngOnInit() {
    let animatedHeader = new AnimatedHeader();
    animatedHeader.init();
  }

}
