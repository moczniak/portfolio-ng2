import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES, ActivatedRoute } from '@angular/router';
import { HTTP_PROVIDERS } from '@angular/http';

import { MockService } from './../../services/mockService';
import { SharedMockService } from './../../services/sharedMockService';

@Component({
  selector: 'mocz-project',
  templateUrl: 'app/components/project/projectDesc.template.html',
  directives: [ROUTER_DIRECTIVES],
  providers: [HTTP_PROVIDERS, MockService]
})

export class ProjectDescComponent implements OnInit {
  project: any;

  constructor(private _mockService: MockService, private _sharedMockService: SharedMockService, private _route: ActivatedRoute) { }


  ngOnInit() {
    let sharedMock = this._sharedMockService.getSingleMock();
    if (typeof sharedMock !== 'undefined') {
      this.project = sharedMock;
    }else {
        this._route.params.subscribe(params => {
          let key = 'projectID';
          let projectID = parseInt(params[key], 10);
          this._mockService.getAllProjects()
                  .subscribe(
                    projects => {
                      let tmpProject = this._mockService.searchForProject(projects, projectID);
                      if (typeof tmpProject !== 'undefined') {
                        this.project = tmpProject;
                      }
                    }
                  );


        });
    }
  }

}
